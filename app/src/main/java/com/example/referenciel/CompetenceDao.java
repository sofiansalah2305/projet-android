package com.example.referenciel;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Index;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface CompetenceDao {
    @Insert
    void insert(Competence competence);

    @Query("DELETE FROM competecnce_table")
    void deleteAll();

    @Query("SELECT * from competecnce_table ORDER BY nomCompetence ASC")
    LiveData<List<Competence>> getToutesCompetences();

}
